<?php
// Include file konfigurasi database
include_once("config.php");

// Cek apakah ada parameter 'id' yang dikirim melalui URL
if (isset($_GET['id'])) {
    $id = $_GET['id'];

    // Hapus data mahasiswa berdasarkan ID
    $query = "DELETE FROM mahasiswa WHERE id = $id";
    $result = mysqli_query($koneksi, $query);

    if ($result) {
        header("Location: index.php");
        exit;
    } else {
        echo "Terjadi kesalahan saat menghapus data mahasiswa.";
    }
} 
?>
